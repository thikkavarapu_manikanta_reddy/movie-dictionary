import React from 'react';
import { useLocation, useNavigate } from "react-router-dom";
import "./MovieDetails.css";
import StarRatings from 'react-star-ratings';

function MovieDetails() {

    let navigate = useNavigate();
    let location = useLocation();
    console.log(location.state);


    return (
        <>
            <div className="container-fluid"><br />
                <div className="row">
                    <div className="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <i className="fa fa-arrow-circle-left" style={{ fontSize: "30px", cursor: "pointer" }} onClick={() => navigate(-1)} ></i>
                        <br /><br />
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <img src={location.state.data.poster} alt="Card image" style={{ width: "75%", height: "75%" }} />
                    </div>
                    <div className="col-12 col-xs-6 col-sm-6 col-md-6 col-lg-6"><br />
                        <div className="row">
                            <div className="col-12 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div style={{ marginTop: "10px" }} />
                                <h4>Title : {location.state.data.title}</h4>
                            </div>
                            <div className="col-12 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <StarRatings
                                    rating={(location.state.data.imdb_rating / 2)}
                                    starDimension="40px"
                                    starRatedColor="yellow"
                                    numberOfStars={5}
                                    name='rating'
                                />
                            </div>
                            <div className="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4>{new Intl.DateTimeFormat("en-US", {
                                    year: "numeric",
                                    month: "short",
                                    day: "2-digit",
                                }).format(new Date(Date.parse(location.state.data.released_on)))} | {location.state.data.length}</h4>
                            </div>
                            <div className="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4>
                                    Director :
                                    {
                                        typeof location.state.data.director == "string" ? location.state.data.director :
                                            <>
                                                {location.state.data.director.map((item, index) => {
                                                    return (
                                                        <span key={index}>
                                                            <span>{item}</span>
                                                            {
                                                                location.state.data.director.length != 1 && index < location.state.data.director.length - 1 ? <span>, </span> : null
                                                            }
                                                        </span>
                                                    )
                                                })}
                                            </>
                                    }
                                </h4>
                            </div>
                            <div className="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4>
                                    Cast :
                                    {
                                        typeof location.state.data.cast == "string" ? location.state.data.cast :
                                            <>
                                                {location.state.data.cast.map((item, index) => {
                                                    return (
                                                        <span key={index}>
                                                            <span>{item}</span>
                                                            {
                                                                location.state.data.cast.length != 1 && index < location.state.data.cast.length - 1 ? <span>, </span> : null
                                                            }
                                                        </span>
                                                    )
                                                })}
                                            </>
                                    }
                                </h4>
                            </div>
                            <div className="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4>Movie Description : {location.state.data.overview}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default MovieDetails
