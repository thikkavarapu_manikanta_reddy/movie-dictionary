import React from 'react';

function Header({ searchMovies, filterMovies }) {
    return (
        <div>
            <div className="row">
                <div className="col-12 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h1>Wookie movies</h1>
                </div>
                <div className="col-12 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div style={{ float: "right" }} className="form-group">
                        <input type="text" className="form-control" placeholder="Search Movies" value={searchMovies} onChange={filterMovies} />
                    </div>
                </div>
            </div>
            <hr />
        </div>
    )
}

export default Header
