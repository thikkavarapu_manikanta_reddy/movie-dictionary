import React, { lazy } from 'react';
import './App.css';
import { Route, Redirect, Routes, Navigate } from 'react-router-dom';
import MovieDetails from './Components/MovieDetails/MovieDetails';
import MovieList from './Components/MovieList/MovieList';

function App() {
  return (
    <Routes>
      <Route exact path="/" element={<MovieList />} />
      <Route path="/movieDetails" element={<MovieDetails />} />
    </Routes>
  );
}

export default App;
